# TechCon Hackathon: AppMod Deploy a Solution

## Cloud Shell Setup

1. Right click on the Open the Google Cloud Console button and click Open link in incognito window.

   ![Image](img/open_console.png)

1. Login to the Google Cloud console using your designated QwikLabs credentials.

1. If prompted, accept the Google Terms of Service.

   ![Image](img/google_tos.png)

1. If prompted, accept the Google Cloud Terms of Service.

   ![Image](img/gc_tos.png)

1. Ensure that the project specified in the lab guide is selected in the project selector near the upper left.

   ![Image](img/project_selected.png)

1. Click the Activate Cloud Shell button in the upper right of the console.

   ![Image](img/activate_cloud_shell.png)

1. Click Continue to activate Cloud Shell

   ![Image](img/cloud_shell_continue.png)

1. Once your Cloud Shell instance is provisioned, verify the project ID is set using the following commands:

   ```bash
   echo ${DEVSHELL_PROJECT_ID}
   ```

   Output should be similar to:

   ```bash output
   $ echo ${DEVSHELL_PROJECT_ID}
   qwiklabs-gcp-01-91c677ec0ecb
   ```

1. Create a work directory using the following commands:

   ```bash
   export WORKDIR=${HOME}/techcon
   mkdir -p ${WORKDIR}
   ```

1. Set the `GCLOUD_USER` environment variable using the following command:

   ```bash
   export GCLOUD_USER=$(gcloud config get-value core/account)
   ```

   If prompted to Authorize Cloud Shell click Authorize

   ![Image](img/authorize_cloud_shell.png)

   Output should be similar to:

   ```bash output
   $ export GCLOUD_USER=$(gcloud config get-value core/account)
   Your active configuration is: [cloudshell-4217]
   ```

1. Download the `var.sh` file using the following command:

   ```bash
   gsutil cp gs://${DEVSHELL_PROJECT_ID}/vars.sh.${GCLOUD_USER%%@*} ${WORKDIR}/vars.sh
   ```

1. Source the `vars.sh` file using the following command:

   ```bash
   source ${WORKDIR}/vars.sh
   ```

1. Configure git using the following command:

   ```bash
   git config --global user.name ${USER}
   git config --global user.email ${USER}@qwiklabs.net
   ```

1. Clone the POC repository using the following command:

   ```bash
   git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop.git ${WORKDIR}/anthos-multicloud-workshop
   ```

1. Install the required tools using the following command:

   ```bash
   mkdir -p ~/.cloudshell && touch ~/.cloudshell/no-apt-get-warning
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/scripts/tools.sh
   ```

1. Setup the necessary files with the `user_setup.sh` script using the following command:

   ```bash
   source ${HOME}/.bashrc
   source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
   ```

## Clusters

- Connect Gateway is configured on the clusters via ACM.
- You can access the EKS clusters in the UI by using your Google identity to log-in

  ![Image](img/log_in_to_cluster.png)

## Current Architecture

![Image](img/current_architecture.png)

## Proposed Production Architecture

![Image](img/prod_architecture.png)
