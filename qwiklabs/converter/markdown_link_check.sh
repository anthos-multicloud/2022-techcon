SCRIPT_PATH="$(
    cd "$(dirname "$0")" >/dev/null 2>&1
    pwd -P
)"
REPO_ROOT=$(realpath ${SCRIPT_PATH}/../..)

BOLD=$(tput bold)
END_COLOR='\e[0m'
GREEN='\e[1;32m'
NORMAL=$(tput sgr0)
RED='\e[1;91m'

echo -e "${BOLD}Pulling ghcr.io/tcort/markdown-link-check:stable image${NORMAL}"
docker pull ghcr.io/tcort/markdown-link-check:stable
echo

script_error_count=0
LABS=$(find ${REPO_ROOT} -maxdepth 1 -type d -regex ".*/lab_[0-9]+.*" | sort)
for lab in ${LABS}; do
    lab_name=$(basename ${lab})
    echo -e "${BOLD}Processing ${lab_name}(${lab})${NORMAL}"
    LAB_FOLDER=${lab}

    docker run --rm -i \
        -v ${LAB_FOLDER}:/data \
        ghcr.io/tcort/markdown-link-check:stable /data/README.md

    return_code=$?
    if [ ! ${return_code} -eq "0" ]; then
        script_error_count=$(($script_error_count + 1))
    fi
    echo

done

if [ ${script_error_count} -eq "0" ]; then
    echo -e "${GREEN}Everything checks out${END_COLOR}"
else
    echo -e "${RED}${script_error_count} labs had errors, review the output above!${END_COLOR}"
fi

exit ${script_error_count}
