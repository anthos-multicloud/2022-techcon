#!/bin/bash -x

source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh

cd ${WORKDIR}
# init git
git config --global user.email "acm@${GCP_PROJECT_ID}"
git config --global user.name "ACM"
if [ ! -d ${HOME}/.ssh ]; then
    mkdir ${HOME}/.ssh
    chmod 700 ${HOME}/.ssh
fi
# pre-grab gitlab public key
ssh-keyscan -t ecdsa-sha2-nistp256 -H gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog >>~/.ssh/known_hosts
git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:platform-admins/config.git

if [ ! -d "${WORKDIR}/config/cluster" ]; then
    mkdir -p ${WORKDIR}/config/cluster
fi

cat <<EOF >/tmp/cgw-clusterrole-impersonate.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: gateway-impersonate
rules:
- apiGroups:
  - ""
  resourceNames:
  - ${GC_USERNAME_1}
  - ${GC_USERNAME_2}
  - ${GC_USERNAME_3}
  - ${GC_USERNAME_4}
  resources:
  - users
  verbs:
  - impersonate
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gateway-impersonate
roleRef:
  kind: ClusterRole
  name: gateway-impersonate
  apiGroup: rbac.authorization.k8s.io
subjects:
- kind: ServiceAccount
  name: connect-agent-sa
  namespace: gke-connect
EOF

cat <<EOF >/tmp/cgw-clusterrole-userbinding.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gateway-cluster-admin
subjects:
- kind: User
  name: ${GC_USERNAME_1}
- kind: User
  name: ${GC_USERNAME_2}
- kind: User
  name: ${GC_USERNAME_3}
- kind: User
  name: ${GC_USERNAME_4}
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
EOF

cp /tmp/cgw-clusterrole-impersonate.yaml ${WORKDIR}/config/cluster
cp /tmp/cgw-clusterrole-userbinding.yaml ${WORKDIR}/config/cluster

pushd config

${WORKDIR}/anthos-multicloud-workshop/platform_admins/scripts/acm_config_repo.sh
cp -pr ${WORKDIR}/anthos-multicloud-workshop/platform_admins/starter_repos/config_init/* .

git add .
git commit -m 'Added initial configuration'
git branch -m master main
git push -u origin main

popd
