from os import environ
import subprocess

class ExecuteCommand:

    def __init__(self, command, environment=None):
        self.command = command
        self.environment = environment

        params = {'stderr': subprocess.PIPE,
                  'stdout': subprocess.PIPE}

        env = environ.copy()
        if self.environment:
            for key, value in environment.items():
                env[key] = value

        params['env'] = env
        params['shell'] = 'True'

        print(f"Executing command: {command}")
        self.result = subprocess.run(self.command, **params)
        self.result.stderr = self.result.stderr.decode("utf-8").splitlines()
        self.result.stdout = self.result.stdout.decode("utf-8").splitlines()

        return