# Copyright 2021 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

def GenerateConfig(context):
    
    prewarm_repo_url = context.properties.get('prewarm_repo_url', 'https://gitlab.com/anthos-multicloud/2022-techcon.git')
    prewarm_repo_branch = context.properties.get('prewarm_repo_branch', 'main')
    prewarm_type = context.properties.get('prewarm_type', 'FULL').upper()

    startup_script = "\n".join([
        "#!/bin/bash",
        "# Copyright 2021 Google Inc. All rights reserved.",
        "#",
        "# Licensed under the Apache License, Version 2.0 (the \"License\");",
        "# you may not use this file except in compliance with the License.",
        "# You may obtain a copy of the License at",
        "#",
        "#     http://www.apache.org/licenses/LICENSE-2.0",
        "#",
        "# Unless required by applicable law or agreed to in writing, software",
        "# distributed under the License is distributed on an \"AS IS\" BASIS,",
        "# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.",
        "# See the License for the specific language governing permissions and",
        "# limitations under the License.",
        "",
        f"export BUILD_DEV={context.properties.get('build_dev', True)}",
        f"export BUILD_GITLAB={context.properties.get('build_gitlab', True)}",
        f"export BUILD_STAGE={context.properties.get('build_stage', True)}",
        f"export BUILD_PROD={context.properties.get('build_prod', True)}",
        "",
        f"export GCP_PROJECT_ID={context.env['project']}",
        f"export GCP_PROJECT_NUMBER={context.env['project_number']}",
        f"export GCLOUD_USER={context.properties['gc_username_1']}",
        f"export GC_USERNAME_1={context.properties['gc_username_1']}",
        f"export GC_USERNAME_2={context.properties['gc_username_2']}",
        f"export GC_USERNAME_3={context.properties['gc_username_3']}",
        f"export GC_USERNAME_4={context.properties['gc_username_4']}",
        f"export AWS_ACCESS_KEY_ID={context.properties['aws_access_key_id']}",
        f"export AWS_SECRET_ACCESS_KEY={context.properties['aws_secret_access_key']}",
        "",
        f"export GCP_ZONE={context.properties['zone']}",
        f"export PREWARM_TYPE={prewarm_type}",
        "",
        "export PREWARM_WORK_DIR=/root/qwiklabs-prewarm",
        f"export PREWARM_REPO_URL={prewarm_repo_url}",
        f"export PREWARM_REPO_BRANCH={prewarm_repo_branch}",
        "rm -rf ${PREWARM_WORK_DIR}",
        "git clone ${PREWARM_REPO_URL} --branch ${PREWARM_REPO_BRANCH} ${PREWARM_WORK_DIR}",
        "",
        "${PREWARM_WORK_DIR}/qwiklabs/prewarm/instance/prewarm_lab.sh"
    ])

    resources = []

    resources.append({
        'name': context.env['name'],
        'type': 'compute.v1.instance',
        'properties': {
            'disks': [{
                'deviceName': 'boot',
                'type': 'PERSISTENT',
                'boot': True,
                'autoDelete': True,
                'initializeParams': {
                    'sourceImage':
                        'projects/ubuntu-os-cloud/global/images/family/ubuntu-2004-lts'
                }
            }],
            'machineType': f"zones/{context.properties['zone']}/machineTypes/n1-standard-1",
            'metadata': {
                'items': [{
                    'key': 'startup-script',
                    'value': startup_script
                }]
            },
            'networkInterfaces': [{
                'accessConfigs': [{
                    'name': 'External NAT',
                    'type': 'ONE_TO_ONE_NAT'
                }],
                'network': 'global/networks/default'
            }],
            'serviceAccounts': [{
                'email': f"{context.env['project']}@{context.env['project']}.iam.gserviceaccount.com",
                'scopes': [
                    'https://www.googleapis.com/auth/cloud-platform'
                ]
            }],
            'zone': context.properties['zone']
        }
    })

    return {'resources': resources}
